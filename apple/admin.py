from django.contrib import admin
from .models import Category, Product, Chat, Profile, Comment, Rating, View


class SlugAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Category, SlugAdmin)
admin.site.register(Product, SlugAdmin)
admin.site.register(Chat)
admin.site.register(Profile)
admin.site.register(Comment)
admin.site.register(Rating)
admin.site.register(View)
