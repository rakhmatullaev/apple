from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django import forms
from django.template.defaultfilters import slugify
from .models import Profile, Category, Product
from autoslug import AutoSlugField


class RegisterForm(UserCreationForm):
    username = forms.CharField(
        label="Username",
        required=True,
        widget=forms.TextInput(attrs={"style": "border: 1px solid #dbdbdb;"}),
    )
    password1 = forms.CharField(
        label="Password",
        required=True,
        widget=forms.PasswordInput(attrs={"class": "form-control mb-20"}),
    )
    password2 = forms.CharField(
        label="Repeat password",
        required=True,
        widget=forms.PasswordInput(attrs={"class": "form-control mb-20"}),
    )
    email = forms.EmailField(
        label="Email",
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control mb-20"}),
    )

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
        )

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            return user


class ProfileForm(forms.ModelForm):
    avatar = forms.ImageField(
        label="Avatar",
        required=False,
        widget=forms.FileInput({"class": "login_input"}),
    )
    phone = forms.CharField(
        label="Phone", required=False, widget=forms.TextInput({"class": "form-control"})
    )
    bio = forms.CharField(
        label="Bio",
        required=False,
        widget=forms.TextInput({"class": "form-control"})
    )

    class Meta:
        model = Profile
        fields = (
            "avatar",
            "phone",
            "bio",
        )

    def save(self, user, commit=True):
        profile = user.profile
        if self.cleaned_data["avatar"] != None:
            profile.avatar = self.cleaned_data["avatar"]
        if self.cleaned_data["phone"] != None:
            profile.phone = self.cleaned_data["phone"]
        if self.cleaned_data["bio"] != None:
            profile.bio = self.cleaned_data["bio"]
        profile.save()
        return profile


class CreateProductForm(forms.ModelForm):
    title = forms.CharField(
        label="Title",
        required=True,
        widget=forms.TextInput(
            attrs={
                "class": "form-control mb-20",
                "placeholder": "Product title",
            }
        ),
    )
    slug = forms.CharField(
        label="Slug",
        required=True,
        widget=forms.TextInput(
            attrs={
                "class": "form-control mb-20",
                "placeholder": "e.g programming-book",
            }
        ),
    )
    image = forms.ImageField(
        label="Image",
        required=True,
        widget=forms.FileInput(attrs={"class": "login_input", "style": "padding: 3px 0 0 3px;"}),
    )
    content = forms.CharField(
        label="Content",
        required=True,
        widget=forms.Textarea(
            attrs={"class": "form-control", "style": "resize: none; height: auto", "placeholder": "Information about product in detail...",}
        ),
    )
    category = forms.ModelChoiceField(
        label="Category",
        queryset=Category.objects.all(),
        widget=forms.Select(attrs={"class": "form-control select2", "placeholder": "e.g Arcade, Books, Toys and more...",}),
    )
    price = forms.DecimalField(
        label="Price",
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "199.99",}),
    )

    class Meta:
        model = Product
        fields = ("title", "slug", "image", "content", "category", "price")

    def save(self, user, commit=True):
        product = super().save(commit=False)
        product.title = self.cleaned_data["title"]
        product.slug = self.cleaned_data["slug"]
        product.author = user
        product.content = self.cleaned_data["content"]
        product.image = self.cleaned_data["image"]
        product.category = self.cleaned_data["category"]
        product.price = self.cleaned_data["price"]
        product.save()
        return product


class UpdateProductForm(forms.ModelForm):
    title = forms.CharField(
        label="title",
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control mb-20"}),
    )

    image = forms.ImageField(
        label="Image",
        required=False,
        widget=forms.FileInput(attrs={"class": "login_input", "style": "padding: 3px 0 0 0;"}),
    )
    content = forms.CharField(
        label="content",
        required=True,
        widget=forms.Textarea(
            attrs={
                "class": "form-control",
                "style": "resize: none;",
            }
        ),
    )
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        widget=forms.Select(attrs={"class": "form-control select2"}),
    )
    price = forms.DecimalField(
        label="price",
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    class Meta:
        model = Product
        fields = ("title", "image", "content", "category", "price")

    def save(self, user, product, commit=True):
        product = product
        product.title = self.cleaned_data["title"]
        product.author = user
        product.content = self.cleaned_data["content"]
        if self.cleaned_data["image"] != None:
            product.image = self.cleaned_data["image"]
        product.category = self.cleaned_data["category"]
        product.price = self.cleaned_data["price"]
        product.save()
        return product