# Generated by Django 3.1.2 on 2020-12-10 22:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apple', '0017_profile_bio'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(blank=True, upload_to='images/%Y/%m/%d', verbose_name='image'),
        ),
    ]
