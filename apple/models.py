from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User
from autoslug import AutoSlugField


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name="Profile")
    avatar = models.ImageField("Avatar", blank=True, upload_to="user/avatars/")
    phone = models.CharField("Phone", max_length=13)
    bio = models.CharField("Bio", max_length=255)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    def get_absoulte_url(self):
        return reverse("profile_detail_url", kwargs={"slug": self.slug})

    def __str__(self):
        return self.user.username


class Category(models.Model):
    title = models.CharField("Title", max_length=255)
    date = models.DateTimeField("Date", default=timezone.now)
    slug = models.SlugField("Link", unique=True)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def current_path(self):
        return "/Categories/" + str(self.id) + "/"

    def get_absoulte_url(self):
        return reverse("category_detail_url", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title


class Product(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Author", default="User"
    )
    title = models.CharField("Title", max_length=255)
    slug = models.SlugField("Link", unique=True)
    image = models.ImageField("image", blank=True, upload_to="images/%Y/%m/%d")
    content = models.TextField("Content")
    price = models.DecimalField("Price", max_digits=100, decimal_places=2, default=0)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True, verbose_name="Category"
    )
    date = models.DateTimeField("Date", default=timezone.now)
    publish = models.BooleanField("Publication", default=True)
    views = models.IntegerField("Views", default=0)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def get_absoulte_url(self):
        return reverse("product_detail_url", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title


class View(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)


class Rating(models.Model):
    products = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name="Product", default=True
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name="User")
    rate = models.DecimalField("Rating", max_digits=5, decimal_places=2, default=5.00)

    def __str__(self):
        return self.products.title


class Chat(models.Model):
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="user", default=True
    )
    created = models.DateTimeField(auto_now_add=True)
    text = models.CharField("text", max_length=250)
    date = models.DateField("date", default=timezone.now)
    sender = models.ForeignKey(
        User, related_name="sended_chats", on_delete=models.CASCADE
    )
    reciever = models.ForeignKey(
        User, related_name="recieved_chats", on_delete=models.CASCADE
    )

    def last_message(self):
        message = self.message_set.latest("created")
        return message.text

    def last_message_date(self):
        message = self.message_set.latest("created")
        return message.created

    def current_path(self):
        return "/chats/" + str(self.id) + "/"

    def update_msg_date(self):
        try:
            self.latest_msg_date = self.message_set.order_by("-created")[0]
            self.save()
        except IndexError:
            pass

    def __str__(self):
        return self.author.username


class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    viewed = models.BooleanField("Viewed message", default=False)
    text = models.TextField(Chat, max_length=500)

    def now(self):
        return int(timezone.now().timestamp() - self.created.timestamp())


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Author")
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name="Product"
    )
    text = models.TextField("Comment")
    date = models.DateTimeField("Date", default=timezone.now)
    publish = models.BooleanField("Publication", default=True)

    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"

    def __str__(self):
        return self.author.username + " | " + self.product.title


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
