from django import template
from django.template import Library
from apple.models import Category

register = Library()


@register.inclusion_tag("apple/categories_header.html")
def categories_header():
    categories = Category.objects.all()
    return {"categories": categories}
