from django import template
from django.template import Library
from apple.models import Category

register = Library()


@register.inclusion_tag("apple/categories_sidebar.html")
def categories_sidebar():
    categories = Category.objects.all()
    return {"categories": categories}