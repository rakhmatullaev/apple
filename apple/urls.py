from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("profile/", views.profile, name="profile"),
    path("signIn/", views.signIn, name="signIn"),
    path("signOut/", views.signOut, name="signOut"),
    path("signUp/", views.signUp, name="signUp"),
    path("chat_preview/", views.chat_preview, name="chat_preview"),
    path("chat_make/make/<int:user_id>/", views.chat_make, name="chat_make"),
    path("chats/<int:chat_id>/", views.chat_detail_url, name="chat_detail_url"),
    path("chats/<int:chat_id>/send-message/", views.send_message, name="send_message"),
    path("profile_edit", views.profile_edit, name="profile_edit"),
    path("search/", views.search, name="search"),
    path("sell/", views.sell, name="sell"),
    path("sell/products/create/", views.product_create, name="product_create"),
    path(
        "sell/products/<slug:slug>/delete/",
        views.product_delete,
        name="product_delete",
    ),
    path("sell/products/<slug:slug>/edit/", views.product_edit, name="product_edit"),
    path("products/<slug:slug>/", views.product_detail, name="product_detail_url"),
    path("profiles/<str:username>/", views.profile_detail, name="profile_detail_url"),
    path("categories/<slug:slug>/", views.category_detail, name="category_detail_url"),
    path("apple/<slug:slug>/comment/", views.comment, name="comment"),
    path(
        "apple/<slug:slug>/comment/<int:pk>/delete/",
        views.comment_delete,
        name="comment_delete",
    ),
]
