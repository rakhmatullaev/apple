from django.shortcuts import render, redirect, reverse
from .models import Product, Category, Profile, Comment, Chat
from django.db.models import Q, Max, Count
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from .forms import RegisterForm, ProfileForm, CreateProductForm, UpdateProductForm
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json


def index(request):
    products = Product.objects.order_by("-date")
    category = Category.objects.all()
    context = {"products": products, "category": category}
    return render(request, "apple/index.html", context)


def profile(request):
    if not request.user.is_authenticated:
        return redirect("signIn")
    user = request.user
    products = user.product_set.order_by("-date")
    views = request.user.view_set.order_by("-date")
    context = {
        "views": views,
        "products": products,
        "user": user,
    }
    return render(request, "apple/profile.html", context)


def product_detail(request, slug):
    product = Product.objects.get(slug__exact=slug)
    if request.user.is_authenticated:
        if not product.view_set.filter(user=request.user).exists():
            product.view_set.create(user=request.user)
        else:
            view = request.user.view_set.get(product=product)
            view.date = timezone.now()
            view.save()
    return render(
        request,
        "apple/product_detail.html",
        {"product": product},
    )


# def sort(request):
# 	query = request.GET.get('sort')
# 	product = Product.objects.order_by(query)
# 	return render(request, 'apple/sort.html', {'product': product, 'query': query})


def profile_detail(request, username):
    user = User.objects.get(username__exact=username)
    products = user.product_set.order_by("-date")
    return render(
        request, "apple/profile_detail.html", {"user": user, "products": products}
    )


def category_detail(request, slug):
    category = Category.objects.get(slug__exact=slug)
    products = category.product_set.annotate(count=Count("view")).order_by("-count")
    return render(
        request,
        "apple/category_detail.html",
        {"category": category, "products": products},
    )


def search(request):
    query = request.GET.get("search")
    products = (
        Product.objects.filter(Q(title__icontains=query))
        .filter(publish=1)
        .order_by("-date")
    )
    return render(request, "apple/search.html", {"query": query, "products": products})


def signIn(request):
    if request.user.is_authenticated:
        return redirect("index")
    message = None
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("index")
        else:
            message = "Invalid user or password, please fill all forms again!"
            return render(request, "apple/signIn.html", {"message": message})
    return render(request, "apple/signIn.html", {"message": message})


def signOut(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("index")


def signUp(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = RegisterForm(request.POST)
            if form.is_valid():
                user = form.save()
                profile = Profile()
                profile.user = user
                profile.phone = "+998 99 000-00-00"
                profile.save()
                login(request, user)
                return redirect("index")
        else:
            form = RegisterForm()
        return render(request, "apple/signUp.html", {"form": form})
    return redirect("index")


def chat_preview(request):
    if not request.user.is_authenticated:
        return redirect("signIn")
    chat_preview = request.user.recieved_chats.annotate(
        max_created=Max("message__created")
    ).order_by("-max_created")
    chat_outside = request.user.sended_chats.annotate(
        max_created=Max("message__created")
    ).order_by("-max_created")
    return render(
        request,
        "apple/chat_preview.html",
        {"chat_outside": chat_outside, "chat_preview": chat_preview},
    )


def chat_make(request, user_id):
    if not request.user.is_authenticated:
        return redirect("signIn")
    if request.user.is_authenticated:
        if request.method == "POST":
            user = User.objects.get(id=user_id)
            if Chat.objects.filter(reciever=user).filter(sender=request.user).exists():
                chat = Chat.objects.filter(reciever=user).get(sender=request.user)
                return redirect(reverse("chat_detail_url", kwargs={"chat_id": chat.id}))
            if request.user.id != user_id:
                user = User.objects.get(id=user_id)
                chat = request.user.sended_chats.create(reciever=user)
                return redirect(reverse("chat_detail_url", kwargs={"chat_id": chat.id}))
    return redirect("index")


def chat_detail_url(request, chat_id):
    chat = Chat.objects.get(id=chat_id)
    chat_preview = request.user.recieved_chats.annotate(
        max_created=Max("message__created")
    ).order_by("-max_created")
    chat_outside = request.user.sended_chats.annotate(
        max_created=Max("message__created")
    ).order_by("-max_created")

    now = timezone.now
    for message in chat.message_set.filter(viewed=False).exclude(user=request.user):
        message.viewed = True
        message.save()

    return render(
        request,
        "apple/chat_detail.html",
        {
            "chat": chat,
            "now": now,
            "chat_preview": chat_preview,
            "chat_outside": chat_outside,
        },
    )


def send_message(request, chat_id):
    chat = Chat.objects.get(id=chat_id)
    if request.method == "POST":
        chat.message_set.create(user=request.user, text=request.POST.get("text"))
    return redirect(reverse("chat_detail_url", kwargs={"chat_id": chat.id}))


def profile_edit(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ProfileForm(request.POST, request.FILES)
            if form.is_valid():
                form.save(request.user)
                return redirect("profile")
        form = ProfileForm(
            initial={
                "phone": request.user.profile.phone,
                "bio": request.user.profile.bio,
            }
        )
        return render(
            request,
            "apple/profile_edit.html",
            {
                "form": form,
            },
        )
    return redirect("index")


def sell(request):
    if not request.user.is_authenticated:
        return redirect("index")
    products = request.user.product_set.all()
    return render(request, "apple/sell.html", {"products": products})


def comment(request, slug):
    product = Product.objects.get(slug__exact=slug)
    if request.method == "POST":
        comment = product.comment_set.create(
            author=request.user, text=request.POST.get("text")
        )
        if request.user.is_superuser:
            comment.publish = True
            comment.save()
    return redirect(reverse("product_detail_url", kwargs={"slug": slug}))


def comment_delete(request, slug, pk):
    product = Product.objects.get(slug__exact=slug)
    comment = product.comment_set.get(id=pk)
    if request.method == "POST":
        comment.delete()
    return redirect(reverse("product_detail_url", kwargs={"slug": slug}))


def favorite(request):
    if not request.user.is_authenticated:
        return redirect("profile_crud")
    favorites = Favorite.objects.filter(user=request.user)
    return render(request, "apple/favorite.html", {"favorites": favorites})


def product_create(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = CreateProductForm(request.POST, request.FILES)
            if form.is_valid():
                form.save(request.user)
                return redirect("sell")
        form = CreateProductForm()
        return render(request, "apple/product_create.html", {"form": form})


def product_edit(request, slug):
    product = Product.objects.get(slug__exact=slug)
    if request.user.is_authenticated:
        if request.method == "POST":
            form = UpdateProductForm(request.POST, request.FILES)
            if form.is_valid():
                form.save(request.user, product)
                return redirect("sell")
        form = UpdateProductForm(
            initial={
                "title": product.title,
                "content": product.content,
                "category": product.category,
                "price": product.price,
            }
        )
        return render(
            request, "apple/product_edit.html", {"form": form, "product": product}
        )


def product_delete(request, slug):
    product = Product.objects.get(slug__exact=slug)
    if request.method == "POST":
        product.delete()
    return redirect(reverse("sell"))
